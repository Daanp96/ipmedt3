window.onload = function() {
  const button = document.getElementsByClassName('js--button');
  const cursorLoad = document.getElementById("js--cursorLoad");
  const kamer = document.getElementById("js--kamer");
  const start = document.getElementById("js--start");
  const knoop = document.getElementById("js--knoop");
  const tutor = document.getElementById("js--tutorial");
  const mannetje = document.getElementById("js--mannetje");
  const draai = document.getElementById("js--draai");

  const stropimage = document.getElementById("js--stropImage");
  const eind = document.getElementById("js--eindscherm");
  const tostart = document.getElementById("js--tostart");

  const stropdas = document.getElementById("js--stropdas");
  const stropdasFourHandsArray = ["LEFT", "RIGHT", "LEFT", "UP","DOWN"];
  const fourHandsTekst = ["Stap 1: Ga met het lange/dikke deel LINKS over het smalle korte deel", "Stap 2 Ga met het lange/dikke deel RECHTS onder de stropdas door", "Stap 3 Ga met het lange/dikke deel weer LINKS over de stropdas", "Stap 4 Ga met het lange/dikke deel OMHOOG  door de lus","Stap 5 Stop het lange/dikke deel door de smalle lus door hem OMLAAG te trekken"];
  const stropdasWinsorArray = ["LEFT", "RIGHT", "LEFT", "UP","LEFT", "UP","DOWN"];
  const winsorTekst = ["Stap 1: Ga met het lange/dikke deel LINKS over het smalle korte deel", "Stap 2 Ga met het lange/dikke deel RECHTS onder de stropdas door", "Stap 3 Ga met het lange/dikke deel weer LINKS over de stropdas", "Stap 4 Ga met het lange/dikke deel OMHOOG  door de lus","Stap 5 Ga met het lange/dikke deel weer LINKS over de stropdas", "Stap 6 Ga met het lange/dikke deel OMHOOG  door de lus","Stap 7 Stop het lange/dikke deel door de smalle lus door hem OMLAAG te trekken"];
  let stap = 0;

  let stropdasKeuze = 1;

  const pijlen = document.getElementsByClassName("js--pijl");

  function stropdasSetup(stropdasArray, tekstArray){
    responsiveVoice.speak(tekstArray[stap], "Dutch Female");
    for (let i = 0; i < pijlen.length; i++) {
      pijlen[i].onmouseenter = (event) =>{
        if(stropdasArray.length > stap){
          if(stropdas.getAttribute("animation-mixer").timeScale != 1){
            if(pijlen[i].getAttribute("id") == ("js--" + stropdasArray[stap])){
              pijlen[i].setAttribute("gltf-model", "assets/pijlGroen.glb");
              stropdas.setAttribute("animation-mixer", "timeScale: 1;");
              setTimeout(function () {
                stropdas.setAttribute("animation-mixer", "timeScale: 0;");
                stap++;
                if(stap == stropdasArray.length){
                  stropdas.setAttribute("gltf-model","assets/StropdasAf.glb");
                }
                responsiveVoice.speak(tekstArray[stap], "Dutch Female");
              }, 3000);
            }else{
              pijlen[i].setAttribute("gltf-model", "assets/pijlRood.glb");
            }
            setTimeout(function () {
              pijlen[i].setAttribute("gltf-model", "assets/pijl.glb");
            }, 1000);
          }
        }
      }
      pijlen[i].onmouseleave = (event) =>{
        setTimeout(function () {
          if(stropdasArray.length == stap){
             eind.setAttribute('visible', 'true');
             kamer.setAttribute("visible", "false");
             if (stropdasKeuze == 1){
               stropimage.setAttribute("src", "assets/four in hand.jpeg");
             } else {
               stropimage.setAttribute("src", "assets/enkele winsor.jpeg");
             }
             tostart.setAttribute("class", "js--button clickable");
             tostart.onclick = (event) => {
               button[0].setAttribute("class", "js--button clickable");
               tostart.setAttribute("class", "js--button");
               eind.setAttribute("visible", "false");
               start.setAttribute("visible", "true");
             }
            for (let i = 0; i < pijlen.length; i++) {
              pijlen[i].setAttribute("class", "js--pijl")
            }
          }
        }, 4000);
      }
    }
    }

    let gedraaid = false;
      draai.onmouseenter = (event) => {

        if (gedraaid == false) {
          stropdas.setAttribute("animation", "property:rotation; dur:500; to:0 90 -65; easing:linear");
          stropdas.setAttribute("animation__2", "property:position; dur:500; to:0.3 3.25 -2.5; easing:linear");
          mannetje.setAttribute("visible", "false");
          pijlen[1].setAttribute("id", "js--LEFT");
          pijlen[3].setAttribute("id", "js--RIGHT");
          gedraaid = true;
        }else if (gedraaid == true) {
          stropdas.setAttribute("animation", "property:rotation; dur:500; to:0 -90 -65; easing:linear");
          stropdas.setAttribute("animation__2", "property:position; dur:500; to:-0.2 3.25 -3.1; easing:linear");
          mannetje.setAttribute("visible", "true");
          pijlen[3].setAttribute("id", "js--LEFT");
          pijlen[1].setAttribute("id", "js--RIGHT");
          gedraaid = false;
        }

      }

  for (let i = 0; i < button.length; i++) {
    button[i].onmouseenter = (event) =>{
      let arc = 0;
      console.log("onmouseenter");
      const loadHandler = setInterval((event)=>{
        if(cursorLoad.getAttribute("arc") >= 360){clearInterval(loadHandler);}
        cursorLoad.setAttribute("arc", arc);
        arc+=2;
      }, 360/1000);

      button[i].onmouseleave = (event) =>{
        clearInterval(loadHandler);
        cursorLoad.setAttribute("arc", 1);
      }
    }

    button[0].onclick = (event) => {
      knoop.setAttribute("visible", "true");
      button[1].setAttribute("class", "js--button clickable");
      button[2].setAttribute("class", "js--button clickable");
      start.setAttribute("visible", "false");
      button[0].setAttribute("class", "js--button");
      stap = 0;
    }
    button[1].onclick = (event) => {
      stropdasKeuze = 1;
      tutor.setAttribute("visible", "true");
      knoop.setAttribute("visible", "false");
      button[3].setAttribute("class", "js--button clickable");
      button[1].setAttribute("class", "js--button");
      button[2].setAttribute("class", "js--button");
    }
    button[2].onclick = (event) => {
      stropdasKeuze = 2;
      tutor.setAttribute("visible", "true");
      knoop.setAttribute("visible", "false");
      button[3].setAttribute("class", "js--button clickable");
      button[1].setAttribute("class", "js--button");
      button[2].setAttribute("class", "js--button");
    }
    button[3].onclick = (event)=> {
      kamer.setAttribute("visible", "true");
      tutor.setAttribute("visible", "false");
      button[3].setAttribute("class", "js--button");
      draai.setAttribute("class", "clickable");
      if(stropdasKeuze == 2){
        stropdas.setAttribute("gltf-model","assets/StropdasWinsor.glb");
        stropdasSetup(stropdasWinsorArray, winsorTekst);
     }else{
       stropdas.setAttribute("gltf-model","assets/Stropdas.glb");
       stropdasSetup(stropdasFourHandsArray, fourHandsTekst);
     }
     for (let i = 0; i < pijlen.length; i++) {
       pijlen[i].setAttribute("class", "js--pijl clickable")
     }
    }
  }
}
